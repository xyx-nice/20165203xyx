import java.util.*;
import java.text.*;

//重写接口中Compare方法
class Compare1 implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Student st1 = (Student)o1;
        Student st2 = (Student)o2;
        int id1 = Integer.parseInt(((Student) o1).getId());
        int id2 = Integer.parseInt(((Student) o2).getId());
        return (id1 - id2);
    }
}

class Compare2 implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Student st1 = (Student) o1;
        Student st2 = (Student) o2;
        if (st1.getTotalScore() == st2.getTotalScore()) {
            return 0;
        }
        if (st1.getTotalScore() > st2.getTotalScore()) {
            return 1;
        }
        if (st1.getTotalScore() <= st2.getTotalScore()) {
            return -1;
        }
        return 0;
    }
}

class StudentTest {
    public static void main(String[] args) {
        List<Student> list = new LinkedList<Student>();//创建一个list链表
        //向链表中添加结点（对象）
        list.add(new Student("20165201", "李梓豪",90,91,95));
        list.add(new Student("20165202", "贾海粟",89,93,92));
        list.add(new Student("20165203", "夏云霄",93,95,92));
        list.add(new Student("20165204", "贾普涵",88,90,91));
        list.add(new Student("20165205", "刘喆君",87,90,92));
        Iterator<Student> iter = list.iterator();//list链表对象用iterator方法获取一个Iterator对象（针对当前链表的迭代器）
        System.out.println("排序前,链表中的数据");
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName() + " 总成绩：" + stu.getTotalScore());
        }
        Collections.sort(list,new Compare1());//调用接口中的Compare1方法来就id来排序
        System.out.println("按学号排序后,链表中的数据");
        iter = list.iterator();
        while (iter.hasNext()){
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName() + " 总成绩：" + stu.getTotalScore());
        }
        Collections.sort(list,new Compare2());//调用接口中的Compare2方法来就总分来排序
        System.out.println("按总成绩排序后,链表中的数据");
        iter = list.iterator();
        while (iter.hasNext()){
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName() + " 总成绩：" + stu.getTotalScore());
        }
    }
}

class Student {
    private char sex;
    private String id;//表示学号
    private String name;//表示姓名
    private int age;//表示年龄
    private double computer_score;//表示计算机课程的成绩
    private double english_score;//表示英语课的成绩
    private double maths_score;//表示数学课的成绩
    private double total_score;// 表示总成绩
    private double ave_score; //表示平均成绩

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Student(String id, String name, char sex, int age) {
        this(id, name);
        this.sex = sex;
        this.age = age;
    }
    public Student(String id, String name, double computer_score, double english_score, double maths_score) {
        this(id, name);
        this.computer_score = computer_score;
        this.english_score = english_score;
        this.maths_score = maths_score;
    }
    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }//获得当前对象的学号，

    public double getComputer_score() {
        return computer_score;
    }//获得当前对象的计算机课程成绩,

    public double getMaths_score() {
        return maths_score;
    }//获得当前对象的数学课程成绩,

    public double getEnglish_score() {
        return english_score;
    }//获得当前对象的英语课程成绩,

    public void setId(String id) {
        this.id = id;
    }// 设置当前对象的id值,

    public void setComputer_score(double computer_score) {
        this.computer_score = computer_score;
    }//设置当前对象的Computer_score值,

    public void setEnglish_score(double english_score) {
        this.english_score = english_score;
    }//设置当前对象的English_score值,

    public void setMaths_score(double maths_score) {
        this.maths_score = maths_score;
    }//设置当前对象的Maths_score值,

    public double getTotalScore() {
        return computer_score + maths_score + english_score;
    }// 计算Computer_score, Maths_score 和English_score 三门课的总成绩。

    public double getAveScore() {
        return getTotalScore() / 3;

    }// 计算Computer_score, Maths_score 和English_score 三门课的平均成绩。

}

class Undergraduate extends Student {
    private String classID;

    public Undergraduate(String id, String name, char sex, int age, String classID) {
        super(id, name, sex, age);
        this.classID = classID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }
}
