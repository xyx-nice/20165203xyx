import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    //赋给对象a12个字符
    StringBuffer a = new StringBuffer("StringBuffer");
    //赋给对象b24个字符
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    //赋给对象c36个字符
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBuffer");
    //赋给对象d48个字符
    StringBuffer d = new StringBuffer("StringBufferStringBufferStringBufferStringBuffer");

    @Test
    public void testcharAt() {
        assertEquals('S',a.charAt(0));
        assertEquals('e',b.charAt(10));
        assertEquals('f',c.charAt(20));
        assertEquals('B',d.charAt(30));
    }
    @Test
    public void testcapactity() {
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
        assertEquals(64,d.capacity());
    }
    @Test
    public void testindexOf() {
        assertEquals(0,a.indexOf("Str"));
        assertEquals(3,a.indexOf("ing"));
        assertEquals(6,a.indexOf("Buffer"));
        assertEquals(8,a.indexOf("ff"));
    }
    @Test
    public void testlength() {
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
        assertEquals(48,d.length());
    }
}

