import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client1 {
    public static void main(String[] args) {
        String mess;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入问题");
        mess=scanner.nextLine();
        String key="";
        int n=-1;
        byte [] a=new byte[128];
        try{  File f=new File("key1.dat");
            InputStream in = new FileInputStream(f);
            while((n=in.read(a,0,100))!=-1) {
                key=key+new String (a,0,n);
            }
            in.close();
        }
        catch(IOException e) {
            System.out.println("File read Error"+e);
        }
        System.out.println("客户端提出的问题为："+mess);
        NewMyBC mybc=new NewMyBC();
        String str;
        mybc.setBeforeExpression(mess);
        str=mybc.transformWay();
        String mi=EncryptDecrypt.AESEncode(key,str);
        System.out.println("客户端加密后密文为："+mi);
        Socket socket;
        DataInputStream in=null;
        DataOutputStream out=null;
        try{  socket=new Socket("localhost",5006);
            in=new DataInputStream(socket.getInputStream());
            out=new DataOutputStream(socket.getOutputStream());
            BufferedReader in1=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer=new PrintWriter(socket.getOutputStream());
            out.writeUTF(mi);
            String  s=in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回答为："+s);
            Thread.sleep(500);
        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}


