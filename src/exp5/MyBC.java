import java.util.*;

public class MyBC {
    String beforeExpression;//未转化的中缀表达式
    String afterExpression = "";//转化后的后缀表达式
    int left = 0;//定义左括号数
    int right = 0;//定义右括号数


    //判断符号的级别
    public int judgeGrade(char chi) {
        int grade = 0;
        switch (chi) {
            //左括号是第一级
            case '(':
                grade = 1;
                break;
            //+和-是第二级
            case '+':
            case '-':
                grade = 2;
                break;
            //*和÷是第三级
            case '*':
            case '÷':
                grade = 3;
                break;
            // "/"是第四级
            case '/':
                grade = 4;
                break;
            //右括号是第五级
            case ')':
                grade = 5;
                break;
            default:
                grade = 0;
        }
        return grade;
    }

    public void setBeforeExpression(String exp) {
        beforeExpression = exp;
    }

    public void transformWay() {
        Stack stack = new Stack();//新建一个空栈
        int i = 0;
        char op;
        while (i < beforeExpression.length()) {
            op = beforeExpression.charAt(i);//op存放从中缀表达式中取到的元素
            if (op >= '0' && op <= '9') {
                afterExpression = afterExpression + op;//如果是数字，则直接输出至后缀表达式
            } else if (op == '+' || op == '-' || op == '*' || op == '÷') {
                afterExpression = afterExpression + ' ';//有运算符，在数字之间加空格
                if (stack.empty()) {//如果栈为空，则运算符直接入栈
                    stack.push(op);
                }
                //根据符号的级别判断操作
                else if (judgeGrade(op) > judgeGrade((char) stack.peek())) {
                    stack.push(op);//比栈顶级别高或相等，直接入栈
                } else {
                    afterExpression = afterExpression + String.valueOf(stack.pop()) + ' ';//否则直接输出
                    i--;
                }

            } else if (op == '(') {
                left++;
                stack.push(op);//左括号直接入栈
            } else if (op == ')') {
                afterExpression += " ";
                right++;
                while ((char) stack.peek() != '(') {//右括号，出栈，直到左括号为止
                    afterExpression = afterExpression + String.valueOf(stack.pop()) + " ";
                }
                stack.pop();
            }
            i++;
        }
        if (left != right) {
            System.out.println("括号有误");
            System.exit(0);
        }


    }
}

