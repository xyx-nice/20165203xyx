import java.io.*;
import java.net.*;
import java.util.*;
public class Server {
    public static void main(String[] args) {
        ServerSocket serverForClient=null;
        Socket socketOnServer=null;
        DataOutputStream out=null;
        DataInputStream in=null;
        try {
            serverForClient=new ServerSocket(5203);
        }
        catch (IOException e1) {
            System.out.println(e1);
            //e1.printStackTrace();
        }
        try {
            System.out.println("等待客户端呼叫");
            socketOnServer=serverForClient.accept();
            out=new DataOutputStream(socketOnServer.getOutputStream());
            in=new DataInputStream(socketOnServer.getInputStream());
            String s=in.readUTF();
            System.out.println("服务器接收到表达式："+s);
            String expr;
            int result;
            NewMyBC mybc=new NewMyBC();
            MyDC mydc=new MyDC();
            mybc.setBeforeExpression(s);
            expr=mybc.transformWay();
            result=mydc.evaluate(expr);
            out.writeUTF("中缀表达式“"+s+"”转后缀表达式“"+expr+"，运算结果为："+result);
            //Thread.sleep(500);
        }
        catch (Exception e2) {
            System.out.println("客户端已断开"+e2);
        }
    }
}

